import React from 'react';
import './App.css';
import {Paper} from "@material-ui/core";
import {FileList} from "./module/files";


const App: React.FC = () => {
    return (
        <div className="App">
            <Paper style={{flex: 1}} className="Main">
                <FileList/>
            </Paper>
        </div>
    );
}

export default App;
