import axios, {AxiosRequestConfig} from 'axios';

export interface MapParams {
    [key: string]: any;
}

export function getConfig(config: AxiosRequestConfig = {}): AxiosRequestConfig {
    return config;
}

export function get(url: string, data?: MapParams, config?: MapParams): Promise<any> {
    return new Promise((resolve, reject) => {
        axios
            .get(addQueryObjectToUrl(url, data), getConfig(config))
            .then(response => resolve(response.data))
            .catch(error => errorHandler(error, resolve, reject, get, [url, data, config]));
    });
}

export function put(url: string, data?: MapParams, config?: MapParams): Promise<any> {
    return new Promise((resolve, reject) => {
        axios
            .put(url, data, getConfig(config))
            .then(response => {
                resolve(response.data);
            })
            .catch(error => errorHandler(error, resolve, reject, put, [url, data, config]));
    });
}

export function post(url: string, data?: MapParams, config?: MapParams): Promise<any> {
    return new Promise((resolve, reject) => {
        axios
            .post(url, data, getConfig(config))
            .then(response => {
                resolve(response.data);
            })
            .catch(error => errorHandler(error, resolve, reject, post, [url, data, config]));
    });
}

export function remove(url: string, data?: MapParams, config?: MapParams): Promise<any> {
    return new Promise((resolve, reject) => {
        axios
            .delete(url, Object.assign(getConfig(config), {data}))
            .then(response => {
                resolve(response.data);
            })
            .catch(error => errorHandler(error, resolve, reject, remove, [url, data, config]));
    });
}

export function request(config?: AxiosRequestConfig): Promise<any> {
    return new Promise((resolve, reject) => {
        axios
            .request(getConfig(config))
            .then(response => {
                resolve(response.data);
            })
            .catch(error => errorHandler(error, resolve, reject, request, [config]));
    });
}

export function patch(url: string, data?: MapParams | string, config?: MapParams): Promise<any> {
    return new Promise((resolve, reject) => {
        axios
            .patch(url, data, getConfig(config))
            .then(response => {
                resolve(response.data);
            })
            .catch(error => errorHandler(error, resolve, reject, patch, [url, data, config]));
    });
}

export function errorHandler(
    error: any,
    resolve: (value?: any) => void,
    reject: (value?: any) => void,
    method: (...args: any[]) => Promise<any>,
    args: any[]
): void {
    reject(error);
}

export function objectToUrlQuery(value: MapParams | undefined | null): string {
    if (!!value && typeof value === 'object' && Object.keys(value).length > 0) {
        const params = encodeURI(
            Object.keys(value)
                .map(key => key + '=' + value[key])
                .join('&')
        );
        return params;
    } else {
        return '';
    }
}

export function addQueryObjectToUrl(url: string, value?: MapParams): string {
    const params = objectToUrlQuery(value);
    if (params) {
        url += (url.match(/\?/) ? '&' : '?') + params;
    }
    return url;
}

export default {get, put, post, patch, delete: remove, request};
