import AsyncService from "./AsyncService";

class WebSocketProfileService implements AsyncService {

    private static instance: WebSocketProfileService;

    websocket?: WebSocket;
    url?: string;
    path?: string;

    subscribe(callback: (message: MessageEvent) => void) {
        if (this.url === undefined && this.path === undefined) return;
        this.websocket = new WebSocket(this.url!!);
        this.websocket.onopen = () => this.websocket ? this.websocket.send(this.path!!) : "";
        this.websocket.onmessage = (message) => callback(message);
    }

    setOnMessage = (callback: (message: MessageEvent) => void) => {
        this.websocket!!.onmessage = (message) => callback(message);
    }

    setUrl = (url: string) => {
        this.url = url
    }

    setPath = (path: string) => {
        this.path = path
    }

    public static getInstance(): WebSocketProfileService {
        if (!WebSocketProfileService.instance) {
            WebSocketProfileService.instance = new WebSocketProfileService();
        }

        return WebSocketProfileService.instance;
    }
}

export default WebSocketProfileService.getInstance();
