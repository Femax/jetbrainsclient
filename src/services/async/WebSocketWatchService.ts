class WebSocketWatchService {

    private static instance: WebSocketWatchService;

    websocket?: WebSocket;
    url?: string;
    path?: string;

    subscribe(callback: (message: MessageEvent) => void) {
        if (this.url === undefined && this.path === undefined) return;
        this.websocket = new WebSocket(this.url!!);
        this.websocket.onopen = () => this.websocket ? this.websocket.send(this.path!!) : "";
        this.websocket.onmessage = (message) => callback(message);
    }

    setOnMessage = (callback: (message: MessageEvent) => void) => {
        this.websocket!!.onmessage = (message) => callback(message);
    }

    setUrl = (url: string) => {
        this.url = url
    }

    setPath = (path: string) => {
        this.path = path
    }

    public static getInstance(): WebSocketWatchService {
        if (!WebSocketWatchService.instance) {
            WebSocketWatchService.instance = new WebSocketWatchService();
        }

        return WebSocketWatchService.instance;
    }
}

export default WebSocketWatchService.getInstance();
