import React from "react";
import FileListView from "./FileListView";
import Api from "../../services/Api";
import File from "../../types/File";
import {createStyles, Snackbar, withStyles} from "@material-ui/core";
import ProfileDialog from "./ProfileDialog";
import FileModelProfile from "../../types/FileModelProfile";
import WebSocketProfileService from "../../services/async/WebSocketProfileService";
import WebSocketWatchService from "../../services/async/WebSocketWatchService";
import {Snac} from "./SnackbarFile";

const styles = createStyles({
    button: {
        margin: 8,
    },
    leftIcon: {
        marginRight: 8,
    },
    rightIcon: {
        marginLeft: 8,
    },
    iconSmall: {
        fontSize: 20,
    },
});

interface Props {
    classes: {
        [key: string]: string
    };
}

interface State {
    data: File[];
    profileDialogOpen: boolean
    profileData: File[];
    snacOpen: boolean
}

class FileList extends React.Component<Props, State> {
    state = {data: [], profileData: [], profileDialogOpen: false, snacOpen: false};

    componentDidMount() {

        this.fetchData()
            .then(data => {
                data.sort((a: File, b: File) => {
                    const numA = a.isDirectory ? 0 : 1;
                    const numB = b.isDirectory ? 0 : 1;
                    return numA - numB;
                })
                this.setState({data: data});
            })
        console.log(decodeURIComponent(window.location.pathname))
        WebSocketWatchService.setPath(window.location.pathname.toString())
        WebSocketWatchService.setUrl("ws://localhost:8080/watchDirectory")
        WebSocketWatchService.subscribe(it => {
            this.setState({
                data: JSON.parse(it.data),
                snacOpen: true,
                profileDialogOpen: false
            })
        })
    }

    onRowClick = (it: FileModelProfile) => {
        it.file.isDirectory && window.location.assign(it.file.filePath)
    }

    profileCurrentPath = () => {
        WebSocketProfileService.setUrl("ws://localhost:8080/profilePath")
        WebSocketProfileService.setPath(window.location.pathname.toString())
        WebSocketProfileService.subscribe(it => {
            const data = JSON.parse(it.data)
            this.setState({
                data: data
            })
        })
    }

    onCloseDialog = () => {
        this.setState({
            profileDialogOpen: false
        })
    }

    onCloseSnac = () => {
        this.setState({
            snacOpen: false
        })
    }

    async fetchData() {
        if (window.location.pathname != "/") return Api.get(
            `http://localhost:8080/`,
            {path: window.location.pathname})
        else return Api.get(`http://localhost:8080/`)
    }

    render() {
        return <div style={{display: 'flex', flexDirection: 'column'}}>
            <div style={{flex: 1}} className="container">
                <FileListView data={this.state.data} onRowClick={this.onRowClick}/>
            </div>
            <ProfileDialog profileData={this.state.profileData}
                           open={this.state.profileDialogOpen}
                           onCloseButton={this.onCloseDialog}/>
            <Snackbar open={this.state.snacOpen}
                      anchorOrigin={{
                          vertical: 'bottom',
                          horizontal: 'left',
                      }}>
                <Snac
                    className={this.props.classes.button}
                    message={"Profile finished"}
                    onClose={this.onCloseSnac}
                    variant={"success"}/>
            </Snackbar>
        </div>;
    }
}

export default withStyles(styles)(FileList)
