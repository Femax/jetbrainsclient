import {makeStyles, Paper, Table, TableBody, TableCell, TableHead, TableRow, withStyles} from "@material-ui/core";
import React from "react";
import FileModelProfile from "../../types/FileModelProfile";
import {Folder, InsertDriveFile} from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    table: {
        minWidth: 700,
    },
}));

interface Props {
    data: FileModelProfile[];
    onRowClick: (file: FileModelProfile) => void;
}

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

const WarningTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: 'yellow',
        },
        '&:nth-of-type(even)': {
            backgroundColor: 'yellow',
        },
    },
}))(TableRow);

const MustBeDeletedTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: 'red',
        },
        '&:nth-of-type(even)': {
            backgroundColor: 'red',
        },
    },
}))(TableRow);

const getFileSizeKiloBytes = (size: number) => {
    return (+size / 1024) + "  kb"
}


const FileListView: React.FC<Props> = (props: Props) => {
    const classes = useStyles();
    console.log(props.data);
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell>Name</StyledTableCell>
                        <StyledTableCell>Size</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.data.map((it, i) => {
                        switch (it.profileStatus) {
                            case 'SUCCESS': {
                                return <StyledTableRow key={i} onClick={() => props.onRowClick(it)}>
                                    <StyledTableCell>{it.file.isDirectory ? <Folder/> :
                                        <InsertDriveFile/>}</StyledTableCell>
                                    <StyledTableCell>{it.file.filePath}</StyledTableCell>
                                    <StyledTableCell>{getFileSizeKiloBytes(it.file.size)}</StyledTableCell>
                                </StyledTableRow>
                            }
                            case 'WARNING' : {
                                return <WarningTableRow key={i} onClick={() => props.onRowClick(it)}>
                                    <StyledTableCell>{it.file.isDirectory ? <Folder/> :
                                        <InsertDriveFile/>}</StyledTableCell>
                                    <StyledTableCell>{it.file.filePath}</StyledTableCell>
                                    <StyledTableCell>{getFileSizeKiloBytes(it.file.size)}</StyledTableCell>
                                </WarningTableRow>
                            }
                            default  : {
                                return <MustBeDeletedTableRow key={i} onClick={() => props.onRowClick(it)}>
                                    <StyledTableCell>{it.file.isDirectory ? <Folder/> :
                                        <InsertDriveFile/>}</StyledTableCell>
                                    <StyledTableCell>{it.file.filePath}</StyledTableCell>
                                    <StyledTableCell>{getFileSizeKiloBytes(it.file.size)}</StyledTableCell>
                                </MustBeDeletedTableRow>
                            }
                        }
                    })}
                </TableBody>
            </Table>
        </Paper>
    )
}

export default FileListView;
