import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FileModelProfile from "../../types/FileModelProfile";
import {CircularProgress, makeStyles} from "@material-ui/core";
//
// const Transition = React.forwardRef(function Transition(props, ref) {
//     return <Slide direction="up" ref={ref} {...props} />;
// });
const useStyles = makeStyles(theme => ({
    progress: {
        margin: theme.spacing(2),
    },
}));

interface Props {
    profileData: FileModelProfile[];
    open: boolean;
    onCloseButton: () => void;
}


export default function ProfileDialog(props: Props) {
    const classes = useStyles();
    const [progress, setProgress] = React.useState(0);

    React.useEffect(() => {
        function tick() {
            // reset when reaching 100%
            setProgress(oldProgress => (oldProgress >= 100 ? 0 : oldProgress + 1));
        }

        const timer = setInterval(tick, 20);
        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <Dialog
            open={props.open}
            maxWidth="sm"
            // fullWidth={true}
            // TransitionComponent={Transition}
            keepMounted
            onClose={props.onCloseButton}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">{"Loading"}</DialogTitle>
            <DialogContent>
                <CircularProgress
                    className={classes.progress}
                    variant="determinate"
                    value={progress}
                    color="secondary"
                />
            </DialogContent>
        </Dialog>
    );
}
