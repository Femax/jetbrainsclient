import File from "./File";

export default interface FileModelProfile {
    file: File,
    profileStatus: 'WARNING' | 'SUCCESS' | 'MUST_BE_DELETED'
}
