export default interface File {
    filePath: string;
    isDirectory: boolean;
    size: number;
}
